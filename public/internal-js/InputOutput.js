function generate_QR(override=null){
  let qrDiv = document.getElementById('QR_Code')
  qrDiv.innerHTML = '';
  utext = document.getElementById('inputURI').value
  if (override) {
    utext = override
  }

  var qrcode = new QRCode(qrDiv, {
    text: utext,
    width: qrDiv.clientWidth,
    height: qrDiv.clientWidth,
    colorDark : "#000000",
    colorLight : "#ffffff",
    correctLevel : QRCode.CorrectLevel.H
  });
}

function removeComment(line){
  commentStartLoc=line.indexOf('//')
  if (commentStartLoc === -1) return line;
  return line.substring(0,commentStartLoc).trim();
}

function removeComments(raw_input){
  let input = ''
  //strip comments
  for(let i = 0; i < raw_input.length; i++){
    line = removeComment(raw_input[i]);
    if (line !== ''){
      input += line
      input += '\n'
      //console.log(line);
    }
  }
  return input
}

function processInput(prettyPrint = false, timeoutForModal=true){
  if(timeoutForModal){
    showProcessingModal()
    //console.log('b', timeoutForModal);
    setTimeout( function(){processInput(prettyPrint = prettyPrint, timeoutForModal=false)},500);
    return
  }
  
  try {
    let raw_input = document.getElementById('inputOnPage').value.split('\n');

    //get filters
    let lT
    lT = 0 - performance.now();
    lT += performance.now();
    console.log('[INFO][PERF] Filters precalculated in', lT/1000, 's')

    //run commands
    lT = 0 - performance.now();
    lT += performance.now();
    console.log('[INFO][PERF] Commands processed in', lT/1000, 's')
    hideProcessingModal();
  }
  catch(err){
    handleError(err);
      let pMF = document.getElementById('processingModal_footer')
      pMF.innerHTML = '<p><b>A Critical unexpected exception occured. Please press force close and check the Progress Log.</b></p><button type="button" data-dismiss="modal" data-target="#processingModal" class="btn btn-warning">Force Close</button>'
  }
}