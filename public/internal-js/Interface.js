//Processing Modal
function enableProcessingModalSpinner(){
  document.getElementById('processingModal_spinner').innerHTML = '<div class="spinner-grow text-primary" ></div>';
}
function disableProcessingModalSpinner(){
  document.getElementById('processingModal_spinner').innerHTML = '';
}
function showProcessingModal(){
  let mTitle = document.getElementById('processingModal_title')
  mTitle.innerHTML = 'Processing Filters & Commands'
  enableProcessingModalSpinner();
  document.getElementById('processingModal_footer').innerHTML = '<button type="button" data-dismiss="modal" data-target="#processingModal" class="btn btn-danger">Force Close</button>'
  $('#processingModal').modal('show')
}

function hideProcessingModal(){
  disableProcessingModalSpinner();
  document.getElementById('processingModal_footer').innerHTML = '<b><p>Processing successfully completed, but this dialog failed to close. Please press force close to continue.</p></b><button type="button" data-dismiss="modal" data-target="#processingModal" class="btn btn-success">Force Close</button>'
  $('#processingModal').modal('hide');
}

//Console/Progress Log Stuff
function consoleOnPage_clear() {
  let pageConsole = document.getElementById('consoleOnPage');
  pageConsole.innerHTML = '';
  initPageConsole();
}

function initPageConsole(){
  let pageConsole = document.getElementById('consoleOnPage');
  pageConsole.innerHTML = '';
  console.log("Initiated:", new Date())
  console.log("User-Agent:", navigator.userAgent)
  console.error("Caught exceptions will be highlighted");
}

//Errorhandling
function handleError(err){
  console.error(err.name, "Caught");
  console.error("Error Message:", err.message);
  console.error("Stack Trace")
  console.error(err['stack']);
}

document.addEventListener("DOMContentLoaded", function(){
      if (!console) {
          console = {};
      }

      //Progress Log
      var logger = document.getElementById('consoleOnPage');

      //Redirect console output --> Progress Log
      console.log = function () {
          let counter = 0
          output = ""
          for(let message of Object.values(arguments)){
            if(counter > 0) output += ' ';
            if (typeof message === 'object') {
                output += (JSON && JSON.stringify ? JSON.stringify(message) : String(message));
            }
            else if (typeof message === 'string') {
              if(message.indexOf('\n') >= 0){
               for(let pmessage of message.split('\n')){
                output += pmessage;
                output +=  '<br />';
                }
              }
              else output += message;
            }
            else {
                output += message;
            }
            counter += 1
          }
          logger.innerHTML += output;
          logger.innerHTML +=  '<br />';
      }

      //Redirect captured errors --> Progress Log
      console.error = function () {
        let counter = 0
        errorOutput = "<span class='bg-danger'>"
        let toast = true;
        for(let message of Object.values(arguments)){
          if(counter > 0) errorOutput += ' ';
          if (typeof message === 'object') {
              errorOutput += (JSON && JSON.stringify ? JSON.stringify(message) : String(message));
          } else {
              errorOutput += message;
              if (message === 'Caught exceptions will be highlighted' ) toast = false;
          }
          counter += 1
        }
        errorOutput +=  `</span>`;
        logger.innerHTML += errorOutput;
        logger.innerHTML +=  '<br />'
        if(toast) {
          $('#notifyError').toast({delay: 5000});
          $('#notifyError').toast('show');
        }
      }

      //Initialize Progress Log
      initPageConsole()

      generate_QR(window.location)
});
