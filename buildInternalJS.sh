#!/bin/bash
OWD=$PWD
outputJSFile="$OWD/public/internal.js"
outputJSFile_external="$OWD/public/internal.js"
internalJSDir="$OWD/public/internal-js"
#externalJSDir="$OWD/public/external-js"
#externalJSScripts="$externalJSDir/jquery.min.js $externalJSDir/xlsx.core.min.js $externalJSDir/Chart.min.js $externalJSDir/popper.min.js $externalJSDir/bootstrap.min.js"
npmBabelDir="$OWD/babel"

mkdir -p $npmBabelDir; cd $npmBabelDir
if [ ! -f 'package.json' ]; then
  yes "" | npm init
fi
npm list @babel/core || npm install --save-dev @babel/core
npm list @babel/cli || npm install --save-dev @babel/cli
npm list @babel/plugin-proposal-class-properties|| npm install --save-dev @babel/plugin-proposal-class-properties

echo "//This file combines all internal custom javascript into 1 file using Babel" > $outputJSFile
printf "//This file was automatically generated on " >> $outputJSFile
date >> $outputJSFile
npx babel --plugins @babel/plugin-proposal-class-properties $internalJSDir >> $outputJSFile
#npx babel --plugins @babel/plugin-proposal-class-properties $externalJSScripts > $outputJSFile_external
